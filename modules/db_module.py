from typing import Dict
import sqlite3
import pandas as pd


def load_data():
    """
    Called at the beginning of the driver program. Creates SQLlite tables for artists and songs, and loads the data
     from both CSV files into each table.
    :param none
    :return: none
     """

    connection = sqlite3.connect(':memory:')
    cursor = connection.cursor()

    create_artists_tbl = """CREATE TABLE artists(
                id INTEGER PRIMARY KEY,
                artist TEXT,
                listeners TEXT,
                song_name TEXT,
                feature TEXT,
                FOREIGN KEY(song_name) REFERENCES songs(song));
                """
    cursor.execute(create_artists_tbl)
   
    create_songs_tbl = """CREATE TABLE songs(
                    id INTEGER PRIMARY KEY,
                    song TEXT,
                    feat TEXT,
                    streams TEXT,
                    ranking TEXT,
                    artist_name TEXT,
                    FOREIGN KEY(artist_name) REFERENCES artists(artist));
                    """
    cursor.execute(create_songs_tbl)
   
    df = pd.read_csv("./data/artists.csv")
    df.to_sql("artists", connection, if_exists='append', index=True, index_label='id')

    df = pd.read_csv("./data/songs.csv")
    df.to_sql("songs", connection, if_exists='append', index=True, index_label='id')

    connection.commit()
    return connection
    

def query_data(connection, query):
    """
    Takes a connection to databass as well as an SQL query string and returns the corresponding result from Databass
    :param connection: The connection to the SQL database
    :param query: A string SQL query from build_query()
    :return: the data from the result of the query
    """
    return connection.cursor().execute(query).fetchall()


valid_terms = {}
valid_terms['artist'] = ['ID: ', 'Artist: ', 'Listeners: ', 'Song: ']
valid_terms['song'] = ['ID: ', 'Song: ', 'Feat: ', 'Streams: ', 'Ranking: ']
def build_query(input_dict: Dict[str, str], join: bool) -> str:
    """
    Takes a dictionary of labeled terms and builds one of three different SQL queries based on
    the length of the input dictionary.
    :param input_dict: A dictionary of labeled terms to use when building the query.
    :return: An SQL query as a string.
    """
    labels = []

    if len(input_dict) == 1: # metadata query
        # query returns the number of rows in the given table
        query = 'SELECT COUNT(' + input_dict['table'] + ') FROM ' + input_dict['table'] + 's;'
        labels.append(f"Number of {input_dict['table']}s: ")

    elif len(input_dict) == 2:  
        query = 'SELECT * FROM ' + input_dict['table'] + 's WHERE ' + input_dict['table'] + '==\'' + input_dict['column'] + '\';'
        labels = valid_terms[input_dict['table']]

    else:
        if join:
            if (input_dict['table'] == 'artist'):
                query = 'SELECT DISTINCT ' + input_dict['target'] + ' FROM ' + input_dict['table'] + 's INNER JOIN songs ON songs.artist_name==\'' + input_dict['column'] + '\';'
            else:
                query = 'SELECT DISTINCT ' + input_dict['target'] + ' FROM ' + input_dict['table'] + 's INNER JOIN artists ON artists.song_name==\'' + input_dict['column'] + '\';'
        else:
            query = 'SELECT ' + input_dict['target'] + ' FROM ' + input_dict['table'] + 's WHERE ' + input_dict['table'] + '==\'' + input_dict['column'] + '\';'
        labels.append(f"{input_dict['target']}: ")

    return query, labels
