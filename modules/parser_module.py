
from typing import Dict

valid_terms = {}
valid_terms['artist'] = ['artist', 'listeners', 'song', 'feature']
valid_terms['song'] = ['song', 'feat', 'streams', 'ranking', 'artist']

def is_valid(input: str):
    """
    Checks whether the user's input is of a valid structure and contains valid terms. If the user's input
    is valid then it also checks whether the equivalent SQL query needs to be a JOIN query.
    :param input: The user's input as a string.
    :return join: True if the resulting query will need to be a JOIN query, otherwise False.
    :return valid: True if the string can be converted into a valid SQL query, otherwise False.
    """

    join = False
    valid = False
    terms = input.split(maxsplit=2) 

    if (0 < len(terms) <= 2) and terms[0] in valid_terms.keys():    # single terms or two term query, only validate table
        valid = True
    
    elif len(terms) == 3:   # targeted query
        if terms[0] in valid_terms.keys():  # query is actually a two term query but the second term has a space
            table = terms[0]
            target = terms[1] + ' ' + terms[2]
            valid = True
            
        elif terms[1] in valid_terms.keys():    # validate table position and value
            table = terms[1]
            target = terms[0]

            for t in valid_terms.keys():
                if target in valid_terms[t]:    # if the targeted value is in any table 
                    valid = True
                    if table != t:  # if the targeted value is not in the table being queried, need to do a JOIN
                        join = True
        
    return valid, join


def parse(input: str) -> Dict[str, str]:
    """
    Parses an input query as a string and splits it into separate terms by a maximum of two spaces. 
    Input terms are then put into a dictionary based on the structure of the input.
    :param input: The user's command-line input as a string.
    :return: A dictionary of labled terms ready to pass to database.Build_query().
    """
    
    fields = input.split(maxsplit=2)    # Split input string by first two spaces, we can do this because we know there will be at most 3 terms in a query

    field_dict = {}
    if len(fields) == 1:    # there is only one term in the input, this will be a metadata query
        field_dict['table'] = fields[0]
    elif (fields[0] == 'song') or (fields[0] == 'artist'):    # the input starts with the table name, tis will be a JOIN query
        field_dict['table'] = fields[0]
        if len(fields) == 2:
            field_dict['column'] = fields[1]
        else:
            field_dict['column'] = fields[1] + ' ' + fields[2]
            
    else:   # there are three terms in the input, this will be a SELECT FROM WHERE query
        field_dict['target'] = fields[0]
        field_dict['table'] = fields[1]
        field_dict['column'] = fields[2]
        
    return field_dict




    