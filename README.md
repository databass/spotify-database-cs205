# Spotify Database CS205
Created by: Brianna Alwell, Walden Ng, Chris Sheehy, Hamzah Elwazir

Our warm-up project, titled <b>Databass</b>, is a wrapper language for storing and retrieving Spotify data in a database.
Our data consists of the top 30 artists on Spotify and their hit songs.

Our program consists of three main components. Our driver handles the user interface, user input, and reads in 2 CSV files, 
'artists.csv' and 'songs.csv'. 'input_parser.py' takes the user input, checks if it represents a valid Databass query. 
Finally, 'database.py' creates a sqlite database, stores the contents of the CSVs inside the appropriate tables, and
turns the Databass query into an SQL query, retrieving the corresponding artist and song info.

<b>How to Run:</b>
1. `python3 databass.py` Database created and loaded on first run.
2. Enter a query. Queries either begin with keyword song or artist as well as the full name.
3. For example: `songs drivers license`
4. Alternatively: `artists olivia rodrigo`
