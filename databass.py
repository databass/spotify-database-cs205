from typing import Dict
from modules import parser_module as prsr
from modules import db_module as db

def main():

    flag = 0
    while True:
        user_input = input("\nPlease enter a query (type 'help' for more info or type 'quit' to exit): ")
        if user_input == 'quit':
            break

        if flag == 0:   # if this is the first time through the loop create database, tables, and fill them with data from csv's 
            connection = db.load_data()
            flag = 1

        if user_input == 'help':
            print_help()

        else:
            valid, join = prsr.is_valid(user_input) # check if the users query is valid and if it requires a JOIN between tables
            if valid:
                query_results, labels = query_db(connection, user_input, join)  # get results from query and appropriate labels for output
                if len(query_results) != 0: # if there was data returned by the query
                    for result, label in zip(query_results[0], labels): # print out returned data with appropriate labels
                        print(label + str(result))
                else:
                    print("No data found in table.")
            else:
                print('Invalid query, please enter a valid query or type \'help\' for more info.\n')

    if flag != 0:
        connection.close()
    print("Thank you for using our databass. Goodbye.")


def print_help():
    print("\nto find out information about a song, please start the query with 'song'")
    print("to find out information about an artist, please start the query with 'artist'")
    print("and example of the syntax would be 'song' 'listens' 'song_name'")
    print("this would return the number of listens for a specific song")
    print("to find out all the information about a song or artist just enter it by itself\n")


def query_db(connection, user_input, join):
    """
    Calls functions to convert user_input to a dictionary of terms, validate those terms, build an SQL query using
    said terms, and query the database with the built query.
    :param connection: The connection to the SQL database.
    :param user_input: The string input by the user.
    :param join: Whether or not the query being built will be a JOIN query.
    :return: the data from the result of the query.
    """

    input_dict = prsr.parse(user_input)
    query, labels = db.build_query(input_dict, join)
    return db.query_data(connection, query), labels


if __name__ == '__main__':
    main()
